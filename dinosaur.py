import pygame
import os
from random import random

from constants import *

dinosaur_images = [pygame.transform.scale(pygame.image.load(os.path.join("assets/run" + str(i) + ".png")),
                                          (IMAGE_SIZE, IMAGE_SIZE)) for i in range(1, 3)]


class Dinosaur:
    def __init__(self, genes=None):
        self.x = IMAGE_SIZE // 2 + 10
        self.y = HEIGHT // 2
        self.images = dinosaur_images
        self.image_index = 0
        self.animation_count = 0
        self.is_jumping = False
        self.jump_count = JUMP_COUNT
        self.score = 0
        self.alive = True
        if genes is None:
            self.genes = [15, 0]  # generate random genes
        else:
            # genes : jump duration (between 1 and 15 frames), cactus visibility (between 1 and 200)
            self.genes = genes

    def move(self, cactus_list):
        # animation
        self.animation_count += 1
        if self.animation_count > 10 // len(self.images):
            self.animation_count = 0
            self.image_index += 1
            if self.image_index >= len(self.images):
                self.image_index = 0

        if self.is_jumping:
            # if the jump is over
            if self.jump_count < JUMP_COUNT - self.genes[0] and self.jump_count > 0:
                self.jump_count = -self.jump_count - 1

            # update y during jump
            if self.jump_count >= -JUMP_COUNT:
                self.y -= (self.jump_count * abs(self.jump_count)) * abs(self.jump_count)/90
                # if self.y > HEIGHT//2:
                #    self.y = HEIGHT//2
                self.jump_count -= 1
            else:
                self.jump_count = JUMP_COUNT
                self.is_jumping = False
        else:
            for cactus in cactus_list:
                if self.x + self.genes[1] < cactus.x + IMAGE_SIZE and self.x + IMAGE_SIZE + self.genes[1] > cactus.x \
                        and self.y < cactus.y + IMAGE_SIZE and self.y + IMAGE_SIZE > cactus.y:
                    self.is_jumping = True

    def collide(self, cactus_list):
        for cactus in cactus_list:
            if self.x < cactus.x + cactus.width // 2 and self.x + IMAGE_SIZE // 2 > cactus.x \
                    and self.y < cactus.y + cactus.height // 2 and self.y + IMAGE_SIZE // 2 > cactus.y:
                return True

    def draw(self, win):
        image = self.images[self.image_index].copy()
        alpha = 128
        image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        win.blit(image, (self.x - IMAGE_SIZE // 2, self.y - IMAGE_SIZE // 2))
