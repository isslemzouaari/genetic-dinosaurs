# Genetic Dinosaurs

Genetic algorithms playing the Chrome dinosaur game

Here's a video of the program :

[![Youtube video : Genetic algorithm for Chrome dinosaur game](http://img.youtube.com/vi/d81q0qIghFE/0.jpg)](http://www.youtube.com/watch?v=d81q0qIghFE "Genetic algorithm for Chrome dinosaur game")
